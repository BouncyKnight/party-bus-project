﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumberGrabber : ISubject{      // The subject

    private List<IObserver> observers;
    private int iNumber;
    private int jNumber;
    private int kNumber;

    public NumberGrabber()
    {
        observers = new List<IObserver>();
    }

    public void NotifyObserver()
    {
        foreach(IObserver observer in observers)
        {
            observer.Update(iNumber, jNumber, kNumber);
        }
    }

    public void Register(IObserver newObserver)
    {
        observers.Add(newObserver);
    }

    public void Unregister(IObserver deleteObserver)
    {
        int observerIndex = observers.IndexOf(deleteObserver);

        Debug.Log("Observer " + (observerIndex + 1) + " deleted.");

        observers.RemoveAt(observerIndex);
    }

    public void SetINumber(int newI)
    {
        iNumber = newI;
        NotifyObserver();
    }

    public void SetJNumber(int newJ)
    {
        jNumber = newJ;
        NotifyObserver();
    }

    public void SetKNumber(int newK)
    {
        kNumber = newK;
        NotifyObserver();
    }

}
