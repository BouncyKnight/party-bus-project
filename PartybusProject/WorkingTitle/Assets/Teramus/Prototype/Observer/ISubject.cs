﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISubject {
    void Register(IObserver o);
    void Unregister(IObserver o);
    void NotifyObserver();
}
