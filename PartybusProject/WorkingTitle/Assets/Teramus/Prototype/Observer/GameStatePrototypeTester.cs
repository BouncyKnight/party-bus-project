﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStatePrototypeTester : MonoBehaviour { // Singleton that can be used as a link to the Observers

    private static GameStatePrototypeTester singletonInstance;

    public static GameStatePrototypeTester instance
    {
        get { return singletonInstance; }
    }


    [HideInInspector] public GameStateGrabber gsGrabber;



    [SerializeField] public List<SimulateGSBase> gameStateScriptList;

    public GameStatePrototypeTester GetInstance()
    {   // If it doesnt exist: set it, if it does but its not this: destroy this then return it, when its not null and is this: return it
        if(singletonInstance != null && singletonInstance != this)  // If a singleton instance already exists and it isnt this script
        {
            Destroy(this.gameObject);       // destroy this object
        }
        else if(singletonInstance == null)  // else if singletonInstance is null
        {
            singletonInstance = this;       // singletonInstance equals this script
        }
        return singletonInstance;   // return singletonInstance
    }

    public void SimulateSetup()
    {
        gsGrabber = new GameStateGrabber();

        GameStateObserver gsInCombat = new GameStateObserver(gsGrabber, GameStatesAvaliable.EnumState.InCombat);

        GameStateObserver gsOutOfCombat = new GameStateObserver(gsGrabber, GameStatesAvaliable.EnumState.OutOfCombat);

        GameStateObserver gsMenu = new GameStateObserver(gsGrabber, GameStatesAvaliable.EnumState.Menu);

        //gsGrabber.SetGameState(GameStatesAvaliable.EnumState.OutOfCombat);
        gsGrabber.SetGameState(GameStatesAvaliable.EnumState.InCombat);
    }

    private void Start()
    {
        GetInstance();
        GetComponents(gameStateScriptList);

        SimulateSetup();
    }

 
}
