﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumberObserver : IObserver // An example of a default observer class to "new"
{
    private int iNumber;
    private int jNumber;
    private int kNumber;

    private static int observerIDTracker = 0;

    private int observerID;

    private ISubject numberGrabber;

    public NumberObserver(ISubject numberGrabber)
    {
        this.numberGrabber = numberGrabber;
        observerID = ++observerIDTracker;

        Debug.Log("New Observer " + observerID);

        numberGrabber.Register(this);
    }

    public void Update(int i, int j, int k)
    {
        iNumber = i;
        jNumber = j;
        kNumber = k;

        PrintNumbers();
    }

    public void PrintNumbers()
    {
        Debug.Log(observerID + " I: " + iNumber + " J: " + jNumber + " K: " + kNumber);
    }
}
