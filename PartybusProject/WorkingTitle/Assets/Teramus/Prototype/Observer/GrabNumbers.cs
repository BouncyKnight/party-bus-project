﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabNumbers : MonoBehaviour
{
    
    public void Simulation()
    {
        NumberGrabber nGrabber = new NumberGrabber();

        NumberObserver observer1 = new NumberObserver(nGrabber);

        nGrabber.SetINumber(197);
        nGrabber.SetJNumber(677);
        nGrabber.SetKNumber(676);

    }

    void Start()
    {
        Simulation();
    }

}
