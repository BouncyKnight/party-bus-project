﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGameStateObserver
{
    void Update(GameStatesAvaliable.EnumState state);       // Will this work?
}

public interface IGameStateSubject
{
    void Register(IGameStateObserver gSO);
    void Unregister(IGameStateObserver gSO);
    void NotifyObserver();
}

public abstract class GameStatesAvaliable
{
    public enum EnumState
    {
        InCombat,
        OutOfCombat,
        Menu
    }

    //public abstract float GetEvent();
}

public class GameStateObserver :  IGameStateObserver  // The basic state observer
{
    protected GameStatesAvaliable.EnumState currentState;    // Is this needed? Currently just used for Debug
    protected GameStatesAvaliable.EnumState targetState;

    protected static int observerIDTracker = 0;

    private int observerID;

    private IGameStateSubject gameStateGrabber;

    public GameStateObserver(IGameStateSubject gameStateGrabber, GameStatesAvaliable.EnumState targetState)
    {   // Calling this constructor registers the Observer
        observerID = ++observerIDTracker;

        this.targetState = targetState;
        Debug.Log("New Observer " + observerID);

        gameStateGrabber.Register(this);

    }

    public void Update(GameStatesAvaliable.EnumState state) // Only called on notification
    {
        if (state == targetState)
        {
            currentState = state;   // Would this be better with a bool?
            Debug.Log(observerID + ": State is target state. " + (state) + " " + (targetState));
        }
        else
        {
            Debug.Log(observerID + ": State is not target state. " + (state) + " " + (targetState));
        }
    }

}

public class GameStateGrabber : IGameStateSubject
{
    private List<IGameStateObserver> gSObservers;

    private GameStatesAvaliable.EnumState newState;

    public GameStateGrabber()
    {
        gSObservers = new List<IGameStateObserver>();
    }

    public void NotifyObserver()
    {
        foreach(IGameStateObserver gObserver in gSObservers)
        {
            gObserver.Update(newState);
        }
    }

    public void Register(IGameStateObserver gSO)
    {
        gSObservers.Add(gSO);

    }

    public void Unregister(IGameStateObserver gSO)
    {
        int observerIndex = gSObservers.IndexOf(gSO);

        Debug.Log("Observer " + (observerIndex + 1) + " deleted.");

        gSObservers.RemoveAt(observerIndex);
    }

    public void SetGameState(GameStatesAvaliable.EnumState newS)
    {
        newState = newS;
        NotifyObserver();
    }


}


