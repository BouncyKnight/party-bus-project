﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectScript : MonoBehaviour {

    public GameObject _infoCanvas;
    [SerializeField] public Text _text;

	// Use this for initialization
	void Start () {
        //_text = _textObject.GetComponent<Text>();
	}

    public void SetText(int h)
    {
        _text.text = h.ToString();
        if(h <= 0)
        {
            _text.text = "Dead";
        }
    }
}
