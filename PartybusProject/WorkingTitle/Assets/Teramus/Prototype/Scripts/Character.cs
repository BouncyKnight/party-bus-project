﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Character {


    protected string _name;
    protected int _health;
    protected int _squadPosition;   // Position in squad line
    protected GameObject _obj;

    // void Attack();

    //virtual void AddActionToQueue() { }
    // void Defend();

    public abstract int CacluateAttack();   // Abstract function allows for functionality to be set in derived class
                                            // Player may use Weapon damage
                                            // Or enemy may use another modifier


    public bool isDead()
    {
        if(_health <= 0)
        {
            return true;
        }
        return false;
    }

    public void TakeDamage(int dmg)
    {
        if ((_health - dmg) <= 0)
        {
            _health = 0;
        }
        else
        {
            _health -= dmg;
        }
        _obj.GetComponent<ObjectScript>().SetText(_health); // Should this be here? It feels a bit clunky as this is an abstract class
    }

    public int GetHealth()
    {
        return _health;
    }

    public string GetName()
    {
        return _name;
    }
    
    //public int GetDamage()
    //{
    //    return _damage;
    //}

    public int GetSquadPosition()
    {
        return _squadPosition;
    }

    public void SetObject(GameObject inObj)
    {
        _obj = inObj;
        if (_obj != null)
        {
            _obj.GetComponent<ObjectScript>().SetText(_health);
        }
    }

    public GameObject GetObject()
    {
        return _obj;
    }
}
