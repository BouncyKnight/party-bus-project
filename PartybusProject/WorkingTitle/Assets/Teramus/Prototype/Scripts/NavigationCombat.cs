﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NavigationCombat : MonoBehaviour {

    public Combat _combatScript;

    public Button _StartButton, _ResetButton, _AttackButton;

    public Text _TurnInfoText;

	// Use this for initialization
	void Start () {
        _ResetButton.gameObject.SetActive(false);
        _AttackButton.gameObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        TurnTextChanger();

        //if (_combatScript.CombatFinished)
        //{
        //    _AttackButton.gameObject.SetActive(false);
        //}
        //else
        //{
        //    _AttackButton.gameObject.SetActive(true);
        //}
	}

    public void CombatStartButton()
    {
        _combatScript.StartPressed = true;
        _StartButton.gameObject.SetActive(false);
        _ResetButton.gameObject.SetActive(true);
        _AttackButton.gameObject.SetActive(true);
    }

    public void CombatResetButton()
    {
        _combatScript.ResetPressed = true;

    }

    public void CombatPlayerAttackButton()
    {
        _combatScript.AttackPressed = true;
    }

    void TurnTextChanger()
    {
        if (_combatScript.PlayerTurnInputPhase)
        {
            _TurnInfoText.text = "Player Turn";
        }
        if (_combatScript.EnemyAttackPhase)
        {
            _TurnInfoText.text = "Enemy Turn";
        }
        if (_combatScript.CombatFinished)
        {
            _TurnInfoText.text = "Combat Over";
        }
    }


}
