﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Character {

    int _weaponDamageMin = 1;
    int _weaponDamageMax = 5;

    public Player(string n, int h, int sp)
    {
        _name = n;
        _health = h;
        _squadPosition = sp;

    }

    public Player()
    {
        _name = "Player";
        _health = 4;
    }


    public override int CacluateAttack()
    {
        int tempDmg = UnityEngine.Random.Range(_weaponDamageMin, _weaponDamageMax);
        return tempDmg;
    }

    //// Use this for initialization
    //void Start () {

    //}

    //   // Update is called once per frame
    //   void Update()
    //   {

    //   }
}
