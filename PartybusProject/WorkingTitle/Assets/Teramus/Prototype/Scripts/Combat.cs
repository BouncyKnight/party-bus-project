﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Combat : MonoBehaviour
{


    // List<Character> _combatants = new List<Character>();    // Stores combatants in current combat

    List<Enemy> _enemySquad = new List<Enemy>();
    List<Player> _playerSquad = new List<Player>();


    PreCombatPrep _pCp;


    Queue<Turn> _turnQueue = new Queue<Turn>();     // Construct Turn then add to Turn queue, then execute based on Turn data

    [SerializeField] protected GameObject _instantiateObject;   // Base Visual Object for combatants

    // Positions on "stage" (Should be handled in own class really)
    public Transform[] _middleRowPos;
    public Transform[] _middleRowPosLeft;
    public Transform[] _middleRowPosRight;
    //

    int amountOfCombatants = 0;

    // Update variables
    float timeElapsed = 0;
    float maxTimeBetweenTurns = 1;
    float turnsElapsed = 0;
    float amountOfTurns = 3;
    bool combatOver = false;


    // UI Intergration Bools
    public bool StartPressed, AttackPressed, ResetPressed;
    public bool PlayerTurnInputPhase;   // Used to show what phase the combat is for UI Text
    public bool EnemyAttackPhase;
    public bool CombatFinished;

    enum CombatPhase
    {
        _PlayerInput,
        _PlayerActionTurn,
        _WaitTime,
        _EnemyActionTurn,
        _CombatCheck,
        _Refresh,
        _CombatFinished
    }

    CombatPhase _CombatPhase;

    // Use this for initialization
    void Start()
    {
        _pCp = GetComponent<PreCombatPrep>();
        //_playerSquad = _pCp.GetPlayerList();
        //_enemySquad = _pCp.GetEnemyList();
        //amountOfCombatants = _playerSquad.Count + _enemySquad.Count;
        //Character myPlayer = new Player();  // Creating a 'new' Player and calling it's instructor


        //myPlayer.TakeDamage();
        //StartOfCombat();
    }

    void Update()
    {
        if (combatOver == false)
        {

            if (StartPressed == true)
            {
                StartPressed = false;
                ResetPressed = false;
                AttackPressed = false;
                StartOfCombat();
            }


            switch (_CombatPhase)
            {

                case CombatPhase._PlayerInput:
                    // Waiting for Player Input
                    if (AttackPressed == true)
                    {


                        AttackPressed = false;
                        turnsElapsed++;
                        Debug.Log("Turn: " + turnsElapsed);
                        AddPlayerTurn();
                        _CombatPhase = CombatPhase._PlayerActionTurn;
                    }
                    break;

                case CombatPhase._PlayerActionTurn:
                    // Executing Turn instructions
                    DamageTurnQueue();
                    CheckCombatantsDead();  // Remove this and fix combatants check for following each attack
                    PlayerTurnInputPhase = false;
                    EnemyAttackPhase = true;

                    _CombatPhase = CombatPhase._WaitTime;
                    break;

                case CombatPhase._WaitTime:
                    // Real-time pause between Action Turns 
                    if (timeElapsed > maxTimeBetweenTurns)
                    {
                        _CombatPhase = CombatPhase._EnemyActionTurn;
                    }
                    timeElapsed += Time.deltaTime;
                    break;

                case CombatPhase._EnemyActionTurn:
                    // Executing Turn instructions
                    AddEnemyTurn();
                    DamageTurnQueue();
                    EnemyAttackPhase = false;
                    _CombatPhase = CombatPhase._CombatCheck;
                    break;

                case CombatPhase._CombatCheck:
                    // Check if combat is over
                    CheckCombatantsDead();
                    _CombatPhase = CombatPhase._Refresh;
                    break;

                case CombatPhase._Refresh:
                    // Refreshes any extra variables for the next Turn loop
                    // Currently isnt called as Combat is already over if ~
                    // ~ combatants are dead
                    timeElapsed = 0;
                    PlayerTurnInputPhase = true;
                    _CombatPhase = CombatPhase._PlayerInput;

                    break;

                case CombatPhase._CombatFinished:   // Current skips
                    CombatFinished = true;
                    PlayerTurnInputPhase = false;
                    EnemyAttackPhase = false;
                    break;

                default:
                    Debug.Log("Error! CombatPhase not set!");
                    break;
            }

        }
        if (ResetPressed)
        {
            ResetPressed = false;
            ResetCombat();
        }
    }

    void StartOfCombat()    // Creates a player object and an enemy object, then instantiates both of them
    {

        _playerSquad = _pCp.GetPlayerList();
        _enemySquad = _pCp.GetEnemyList();
        amountOfCombatants = _playerSquad.Count + _enemySquad.Count;

        for (int i = 0; i < _playerSquad.Count; i++)
        {
            _playerSquad[i].SetObject(Instantiate(_instantiateObject, _middleRowPosLeft[i].position, transform.rotation));
            _playerSquad[i].GetObject().name = _playerSquad[i].GetObject().name + " " + i;
        }

        for (int j = 0; j < _enemySquad.Count; j++)
        {
            _enemySquad[j].SetObject(Instantiate(_instantiateObject, _middleRowPosRight[j].position, transform.rotation));
        }

        _CombatPhase = CombatPhase._Refresh;

    }

    void DamageTurnQueue()
    {
        int count = _turnQueue.Count;
        for (int i = 0; i < count; i++)
        {

            Turn tempTurn = _turnQueue.Dequeue();
            int tempDmg = tempTurn.GetResultDamage();

            tempTurn.GetDefendingCharacter().TakeDamage(tempDmg);
            Debug.Log(tempTurn.GetAttackingCharacter().GetName() + " attacks: " + tempTurn.GetDefendingCharacter().GetName() + " dealing " + tempTurn.GetResultDamage().ToString());
            // Debug.Log(tempTurn.GetDefendingCharacter().GetName() + ": " + tempTurn.GetDefendingCharacter().GetHealth());

            if (tempTurn.GetDefendingCharacter().isDead())
            {
                tempTurn.GetDefendingCharacter().GetObject().GetComponent<Renderer>().material.color = new Color(100, 0, 0, 255);
            }
        }

    }

    void TurnAddOneAfterAnother()
    {
        int j = 0;
        int l = 0;

        for (int i = 0; i < _playerSquad.Count; i++)
        {
            if (j > _enemySquad.Count)
            {
                j = 0;
            }
            Turn OaA = new Turn(_playerSquad[i], _enemySquad[j]);
            _turnQueue.Enqueue(OaA);
            j++;
        }

        for (int k = 0; k < _enemySquad.Count; k++)
        {
            if (l > _playerSquad.Count)
            {
                l = 0;
            }
            Turn OaA2 = new Turn(_enemySquad[k], _playerSquad[l]);
            _turnQueue.Enqueue(OaA2);
            l++;
        }
    }

    void AddPlayerTurn()
    {
        int i = 0;
        int j = 0;
        for(i = 0; i <_playerSquad.Count; i++)
        {
            if (j > _enemySquad.Count)
            {
                j = 0;
            }
            Turn PlayerTurn = new Turn(_playerSquad[i], _enemySquad[j]);
            _turnQueue.Enqueue(PlayerTurn);
            j++;
        }
    }

    void AddEnemyTurn()
    {
        int i = 0;
        int j = 0;
        for (i = 0; i < _enemySquad.Count; i++)
        {
            if (j > _playerSquad.Count)
            {
                j = 0;
            }
            Turn PlayerTurn = new Turn(_enemySquad[i], _playerSquad[j]);
            _turnQueue.Enqueue(PlayerTurn);
            j++;
        }
    }

    void CheckCombatantsDead()
    {
        // Dead Checkers Start
        int amountOfPlayersDead = 0;
        foreach (Player p in _playerSquad)
        {
            if (p.isDead())
            {
                amountOfPlayersDead++;
            }
        }

        int amountOfEnemiesDead = 0;
        foreach (Enemy e in _enemySquad)
        {
            if (e.isDead())
            {
                amountOfEnemiesDead++;
            }
        }
        // Dead Checkers End

        // CombatOver Condition Checker
        if (amountOfPlayersDead >= _playerSquad.Count || amountOfEnemiesDead >= _enemySquad.Count)
        {
            combatOver = true;
            CombatFinished = true;
            PlayerTurnInputPhase = false;
            EnemyAttackPhase = false;
        }
    }

    void ResetCombat()  // Version issue, worked on earlier version
    {                   // Currently works, doesnt cause memory leak (somehow?!?)
        RemoveObjectsForReset();    // Doesnt delete objects properly

        _playerSquad.Clear();       // Clears correctly
        _enemySquad.Clear();
        _playerSquad = new List<Player>();  // Recreates list as a fresh list
        _enemySquad = new List<Enemy>();
        turnsElapsed = 0;
        timeElapsed = 0;
        CombatFinished = false;

        combatOver = false;

        _pCp.AddCombatantsDefault();   // Instantly refils _playersquad and _enemysquad without being told to
                                        // ~ No clue why
        StartOfCombat();
    }

    void RemoveObjectsForReset()
    {
        foreach(Player p in _playerSquad)
        {
            //GameObject g = p.GetObject();
            Destroy(p.GetObject());
            p.SetObject(null);
        }

        foreach(Enemy e in _enemySquad)
        {
            Destroy(e.GetObject());
            e.SetObject(null);
        }
    }


    // For later usage  // Kept for archive purposes
    //void DamageTurn()
    //{
    //    for (int i = 0; i < _combatants.Count; i++)
    //    {
    //        if (_combatants[i].isDead())
    //        {
    //            Color red = new Color(100, 0, 0, 255);
    //            _combatants[i].GetObject().GetComponent<Renderer>().material.color = red;
    //            continue;
    //        }
    //        else
    //        {
    //            int tempDmg = _combatants[i].CacluateAttack();
    //            _combatants[i].TakeDamage(tempDmg);

    //            Debug.Log(_combatants[i].GetName() + ": " + _combatants[i].GetHealth());
    //        }

    //    }

    //}

    //void CalculateOrder()
    //{
    //    //int tempInt = 0;
    //    for (int i = 0; i < _combatants.Count; i++)
    //    {

    //    }
    //} 


}
