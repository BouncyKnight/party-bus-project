﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turn { // Used for storing turn activities

    Character _attackingCharacter;
    Character _defendingCharacter;
    int _inputDamage;


    public Turn(Character _aC, Character _tC)   // Constructor creates object with Attacking Character and Defending Character
    {
        _attackingCharacter = _aC;
        _defendingCharacter = _tC;
        _inputDamage = _attackingCharacter.CacluateAttack();
    }

    public int GetResultDamage()    // Later expansion will be for damage modifiers based on Turn's Character variable data
    {
        int _resultDamage = _inputDamage;
        return _resultDamage;
    }

    public Character GetAttackingCharacter()
    {
        return _attackingCharacter;
    }

    public Character GetDefendingCharacter()
    {
        return _defendingCharacter;
    }

}
