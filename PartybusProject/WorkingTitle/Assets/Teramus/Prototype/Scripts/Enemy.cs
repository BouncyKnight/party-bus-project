﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Character {

    protected int _damage = 1;

    public Enemy(string n, int h, int sp)
    {
        _name = n;
        _health = h;
        _squadPosition = sp;
    }

    public Enemy()
    {
        _name = "Enemy";
        _health = 10;
        _squadPosition = 1;
    }

    public override int CacluateAttack()
    {
        return _damage;
    }
}
