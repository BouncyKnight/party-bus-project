﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreCombatPrep : MonoBehaviour {

    List<Player> _combatPlayers = new List<Player>();

    public int numberOfPlayers;
    public int numberOfEnemies;

    List<Enemy> _combatEnemies = new List<Enemy>();

	// Use this for initialization
	void Start () {

        AddCombatantsDefault();

        GetComponent<Combat>().enabled = true;

        //for(int i = 0; i < numberOfPlayers; i++)
        //{
        //    AddPlayerToCombatPlayers(new Player());
        //}

        //for(int j = 0; j < numberOfEnemies; j++)
        //{
        //    AddEnemyToCombatEnemies(new Enemy());
        //}
	}

    public void AddPlayerToCombatPlayers(Player inputPlayer)    // For future potential use
    {
        Player _p = inputPlayer;
        _combatPlayers.Add(_p);
    } 

    public void AddEnemyToCombatEnemies(Enemy inputEnemy) // For future potential use
    {
        Enemy _e = inputEnemy;
        _combatEnemies.Add(_e);
    }

    public void AddCombatantsDefault()   // For UI Debug/Presentation
    {
        for (int i = 0; i < numberOfPlayers; i++)
        {
            AddPlayerToCombatPlayers(new Player());
        }

        for (int j = 0; j < numberOfEnemies; j++)
        {
            AddEnemyToCombatEnemies(new Enemy());
        }
    }

    public List<Player> GetPlayerList()
    {
        return _combatPlayers;
    }

    public List<Enemy> GetEnemyList()
    {
        return _combatEnemies;
    }



}
