﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

abstract public class Item {    // Base parent class for all "Items", e.g. Guns, Loot, etc
                                // Consider creating an Item Database to store info of items, 
                                // then only keeping an ID to the object in inventory, calling directly when needed
    protected string name;  
    protected int value;
    // protected Image icon;    // Consider creating a Database for Icons and a struct to pass the correct data back

    abstract public void Speak(); // Has to be overriden by any child classes
}

public class Gun : Item         // Child class of Item
{

    protected int damage = 2;   // Variable unique to Gun, Default values that are the same regardless
    protected int accuracy = 90;    // Variable unique to Gun, Default values that are the same regardless

    public Gun()    // Default constructor
    {
        name = "Default";
        value = 5;
    }

    public Gun(string s)    // Overloaded Constructor (string s)
    {
        name = s;
        value = 10;
    }

    public Gun(string s = "Default", int v = 15)    // Overloaded Constructor (string s, int v)
    {
        name = s;
        value = v;
    }

    public override void Speak()    // Overriden version of speak, personalised for Gun
    {
        Debug.Log("Name: " + name + " Cost: " + value + " Damage: " + damage + " Accuracy: " + accuracy );
    }

    ~Gun()  // Called on deletion of object
    {
        Debug.Log("Deleted " + name + " gun");
    }
}

public class Armour : Item
{
    protected int armourClass = 0;  // Variable unique to Armour, Default value set


    public Armour() // Default Constructor
    {
        name = "Default";
        value = 10;
        armourClass = 1;
    }

    public Armour(string s) // Overloaded Constructor (string s)
    {
        name = s;
        value = 15;
        armourClass = 2;
    }
    
    public Armour(string s, int v)  // Overloaded Constructor (string s, int v)
    {
        name = s;
        value = v;
        armourClass = 3;
    }

    public Armour(string s, int v, int ac)  // Overloaded Constructor (string s, int v, int ac)
    {
        name = s;
        value = v;
        armourClass = ac;
    }

    public override void Speak()    // Overriden version of speak, personalised for Armour
    {
        Debug.Log("Name: " + name + " Cost: " + value + " AC: " + armourClass);
    }

    ~Armour()   // Called on deletion of object
    {
        Debug.Log("Deleted " + name + " armour");
    }
}