﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// https://stackoverflow.com/questions/15035477/rpg-inventory-adding-and-deleting-items
// https://answers.unity.com/questions/665317/how-to-resize-c-array-of-gameobjects.html

// Bulk of code originally from these sources, study it and rework it as your own

public class Inventory : MonoBehaviour {
    public Item[] items;                        // Consider reworking this to be a different data structure
                                                // Maybe utilising a dictionary or map
    public int Capacity
    { get
        {   // if items == null return 0
            if (items == null)
            {
                return 0;
            }
            else        // Else return items.Length
            {
                return items.Length;
            }
        }
    }

    public Inventory(int capacity = 10) // Create a default size capacity if none is set
    {
        SetInventorySize(capacity);
    }

    ~Inventory()                        // Runs on destruction of object
    {                                   // Consider calling the Garbage Collector more often with IDisposable interface
        Debug.Log("Array deleted");
    }

    public void SetInventorySize(int newSize)   // Change size of inventory
    {
        if(newSize <= 0)        // Checks if new size is less than or equal to 0
        {
            items = null;       // If it is then set items[] to null, loose data will be caught by the garbage collector
        }
        else if(items == null)  // If items[] is currently equal to null
        {
            items = new Item[newSize];  // Create a new items[] array with 
        }
        else                            // Otherwise
        {
            Item[] temp = new Item[newSize];    // Create a temporary Item[] array at size of newSize
            for(int i = 0; i < Mathf.Min(newSize, items.Length); i++)   // Iterate through each position, with the limit 
            {                                                           // ~depending on which is lower, newSize or items.length 
                                                                        // Ensures that temp[] doesn't try to copy something outside of items[]'s index
                temp[i] = items[i];                                     // Sets temp[i] object to be the same as the object at items[i] position
            }
            items = temp;   // Sets items[] array to be the same as temp[], thus changing the size of items[] to temp[]'s size
                            // Loose data is collected by the garbage collector
        }
    }

    public int CheckForFirstAvailableSlot() // Cycles through items[] for the first available spot
    {
        if(items != null)       // if items[] is not equal to null
        {
            for(int i = 0; i < items.Length; i++)
            {
                if(items[i] == null)    // if i's position in items[] is null or 'empty'
                {
                    return i;   // return that position
                }
            }
        }
        return -1;  // if a position is not found, return -1. Showing that there was not a slot free
    }

    public int AddItem(Item item)   // Cycles through items[] and adds at the next available slot
    {
        if(items != null)           // if items[] != null
        {
            for(int i = 0; i < items.Length; i++)
            {
                if(items[i] == null)    // if i's position in items[] equals null
                {
                    items[i] = item;    // i's position is the added items position
                    return i;           // return that position
                }
            }
        }
        
        item = null;    // Primes the item for garbage collection as C# does not have a 'delete' keyword
        
        return -1;  // if a position is not found, return -1. Showing that there was not a slot free
    }

    public int SortInventory()  // Would help for functionality
    {
        return 1;
        
    }

    private void Start()
    {                           // Series of test cases
        items = new Item[10];       // items is the inventory storage with a size of 10
        Debug.Log(items.Length);    // Display the size of items[]

        SetInventorySize(5);        // Set items[] size to 5, removing 5 positions

        Debug.Log(items.Length);    // Display new size of items[]

        AddItem(new Gun());         // Add a new Gun object to items[] in the first slot free, is collected by garbage collection if no room
        items[0].Speak();           // Display info about the object at items[0]

        Debug.Log("Next item slot at: " + CheckForFirstAvailableSlot());    // Display the index of the first slot available, displays -1 if there are none
        AddItem(new Gun("Justice", 20));    // Add a new Gun object with a custom name and cost to items[]
        AddItem(new Gun("PewPew"));         // Add a new Gun object with a custom name to items[]

        AddItem(new Gun());                 // Add a new Gun object with default values to items[]
        AddItem(new Gun("Smooth Shooter")); // Add a new Gun object with a custom name to items[]

        for (int i = 0; i < items.Length; i++)  // Iterates through items[]
        {
            if (items[i] != null)   // If the current iterations position in the items[] index is not empty or null
            {
                items[i].Speak();   // Display info about the object at the index position items[i]
            }
        }

        Debug.Log("Next item slot at: " + CheckForFirstAvailableSlot());    // Display the index of the first slot available, displays -1 if there are none
        Debug.Log(items.Length);    // Display current size of items[]
        SetInventorySize(7);        // Set items[] size to 7, creating 2 empty positions
        Debug.Log(items.Length);    // Display new size of items[]

        AddItem(new Armour());      // Adds a new Armour object with default values to items[]
        AddItem(new Armour("Fancy Hat", 20, 3));    // Adds a new Armour object with a custom name, cost and armour class to items[]

        AddItem(new Armour("Large Speedo", 1, 0));    // Adds a new Armour object with a custom name, cost and armour class to items[],  
                                                      // This will fail as items[] is already full

        for (int i = 0; i < items.Length; i++)  // Iterates through items[]
        {
            if (items[i] != null)   // If the current iterations position in the items[] index is not empty or null
            {
                items[i].Speak();   // Display info about the object at the index position items[i]
            }
        }                           // "Large Speedo" will not be displayed as it was not added to items[]

        SetInventorySize(1);        // Set items[] size to 1, releasing 6 data entries, loose data collected by garbage collector
        Debug.Log(items.Length);    // Display new size of items[]
        items[0].Speak();           // Display info about the object at the index position items[0]

    }
}
