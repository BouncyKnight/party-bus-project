﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SimulateCombat : SimulateGSBase {

    public GameObject block;


    GameObject g;


    void Start () {


        StateController.GetInstance.onStateChangedEvent += onStateChangedEvent;	// Subscribed to event
        StateController.GetInstance.setState(GAMESTATE.INCOMBAT);
	}
	
	public void onStateChangedEvent(GAMESTATE state)
	{
		if (state == GAMESTATE.INCOMBAT) {
			// This is the method that is AUTOMATICALLY called for us by the
			// StateController class because we've subscribed to the event
			g = Instantiate (block);
		} else if(state != GAMESTATE.INCOMBAT && g != null){
			Destroy (g);
		}
	}

    private void OnDisable()
    {
        //StateController.GetInstance.onStateChangedEvent -= onStateChangedEvent;

        //SceneManager.LoadScene("StatePointNClick", LoadSceneMode.Additive);
        StateController.GetInstance.setState(GAMESTATE.POINTNCLICK);    // Work out the order for this
    }
}
