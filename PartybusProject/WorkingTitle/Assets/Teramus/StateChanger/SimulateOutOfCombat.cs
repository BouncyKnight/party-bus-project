﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SimulateOutOfCombat : SimulateGSBase {


	void Start(){
		StateController.GetInstance.onStateChangedEvent += onStateChangedEvent;

	}

	public void onStateChangedEvent(GAMESTATE state)
	{
		if (state == GAMESTATE.POINTNCLICK) {
			Debug.Log("The new state is: " + state);
		}
		// This is the method that is AUTOMATICALLY called for us by the
		// StateController class because we've subscribed to the event

	}


	private void OnDestroy()
    {
        StateController.GetInstance.onStateChangedEvent -= onStateChangedEvent;
    }

    public void OnDisable()
    {

       // SceneManager.LoadScene("StateCombat",LoadSceneMode.Single);
        StateController.GetInstance.setState(GAMESTATE.INCOMBAT);
    }
}
