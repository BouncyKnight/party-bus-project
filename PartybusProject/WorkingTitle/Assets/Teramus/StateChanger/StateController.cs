﻿using UnityEngine;      // Base of code provided by Craig Fortune

public enum GAMESTATE
{
    MENU,
    INCOMBAT,
    POINTNCLICK
}

public class StateController {

    public event OnStateChangedEvent onStateChangedEvent;
    public delegate void OnStateChangedEvent(GAMESTATE state);

    private static StateController instance;

    public static StateController GetInstance
    {
        get
        {
            if(instance == null)
            {
                instance = new StateController();
            }
            return instance;
        }
    }


    protected GAMESTATE _state;
    protected float _startedStateTime;

    // GAMESTATE MENU is the default Enum for GAMESTATE

    public StateController()
    {
        //setState(GAMESTATE.INCOMBAT);

    }
    
    public void setState(GAMESTATE newState)
    {
        Debug.Log("Current State is: " + _state);
        if (_state == newState)
        {
            return;
        }
        else
        {

            _state = newState;
            _startedStateTime = Time.time;

            if(onStateChangedEvent != null)
            {
                Debug.Log("New State is: " + newState);
                onStateChangedEvent(_state);
            }
        }
    }
}
