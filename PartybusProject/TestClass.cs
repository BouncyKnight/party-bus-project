using UnityEngine;      // Code provided by Craig Fortune

public class TestClass : MonoBehaviour
{
    public SimplifiedStateController _stateController;

    void Start()
    {
        // Assume _stateController is already a reference to a SimplifiedStateController object
        // This is how we subscribe
        _stateController.onStateChangedEvent += onStateChangedEvent;
    }

    public void onStateChangedEvent(GAMESTATE state)
    {
        // This is the method that is AUTOMATICALLY called for us by the
        // SimplifiedStateController class because we've subscribed to the event
        
        Debug.Log("The new state is: " + state);
    }

    void OnDestroy()
    {
        // Assume _stateController is already a SimplifiedStateController object
        // This is how we unsubscribe
        _stateController.onStateChangedEvent -= onStateChangedEvent;
    }
}