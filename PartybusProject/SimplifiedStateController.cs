using UnityEngine;      // Code provided by Craig Fortune

public enum GAMESTATE
{
    START,
    PLAYING,
    GAMEOVER
}

public class SimplifiedStateController : MonoBehaviour
{
    public event OnStateChangedEvent onStateChangedEvent;
    public delegate void OnStateChangedEvent(GAMESTATE state);

    protected GAMESTATE _state;
    protected float _enteredStateTime;

    void Start()
    {
        setState(GAMESTATE.START);
    }

    public void setState(GAMESTATE newState)
    {
        if(_state == newState)
        {
            // Don't bother channging
            return;
        }
        else
        {
            _state = newState;
            _enteredStateTime = Time.time;
            // Debug.Log("New state: " + _state);

            // This is actual bit that triggers the event
            // If this is null, then nothing is subscribed
            if(onStateChangedEvent != null)
                onStateChangedEvent(_state);
        }
    }

    // Assume more methods for getState, getStateEnterTime etc here
}